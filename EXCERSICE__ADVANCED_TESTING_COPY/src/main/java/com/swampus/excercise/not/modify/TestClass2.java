package com.swampus.excercise.not.modify;

public class TestClass2 {
    private TestClass1 testClass1;

    public TestClass2(TestClass1 testClass1) {
        this.testClass1 = testClass1;

    }

    public int method(int a){
        return testClass1.addFive(a);
    }

    public int method2(int a){

        return testClass1.throwException(a)+1;
    }
}
