package com.swampus.excercise.not.modify;

public class TestClass1 {

    public int addFive(int a){
        return a+5;
    }

    public int throwException(int a){
        throw new IllegalArgumentException();
    }
}
