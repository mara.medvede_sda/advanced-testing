package com.swampus.excercise.not.modify;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TestClass2Test {
    private TestClass1 testClass1 = spy(new TestClass1());
    private TestClass2 testClass2 = new TestClass2(testClass1);

    @Test
    public void method() {
        assertEquals(15, testClass2.method(10));

    }

    @Test
    public void method2() {
        Mockito.doReturn(5).when(testClass1).throwException(5);
        assertEquals(6, testClass2.method2(5));


    }
}