package com.swampus.excercise.task;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class JuniorTask01Test {
private JuniorTask01 juniorTask01 = new JuniorTask01();

    @Test
    public void calculateInterests() {
        assertEquals(5, juniorTask01.calculateInterests(0));
        assertEquals(3155, juniorTask01.calculateInterests(3000));
    }

    @Test
    public void createEncryptedString() {
        assertEquals("HAYSFUASCTestHAYSFUASC",juniorTask01.createEncryptedString("Test"));
    }

    @Test
    public void checkNotNegative() {
        juniorTask01.checkNotNegative(5);

        try {
            juniorTask01.checkNotNegative(-5);
            fail();
        }catch (RuntimeException e){
            assertEquals("Negative!", e.getMessage());
        }

    }

    @Test
    public void containsTwoSimilarElements() {
        List<String> stringList = new ArrayList<>();
        stringList.add("Mara Medvede");
        stringList.add("Janis Berzins");
        stringList.add("Alina");
        stringList.add("abcDEF");
        stringList.add("Mara Medvede");
        stringList.add("abcDEF");

        assertTrue(juniorTask01.containsTwoSimilarElements(stringList));

        stringList.clear();
        stringList.add("Mara Medvede");
        stringList.add("Mara Medvede");

        assertFalse(juniorTask01.containsTwoSimilarElements(stringList));
    }
}