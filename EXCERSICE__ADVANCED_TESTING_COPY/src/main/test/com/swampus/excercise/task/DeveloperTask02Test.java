package com.swampus.excercise.task;

import com.swampus.excercise.not.modify.UCMAnalyzer;
import org.junit.Test;

import static org.junit.Assert.*;

public class DeveloperTask02Test {
    private DeveloperTask02 developerTask02 = new DeveloperTask02(new UCMAnalyzer());

    @Test
    public void doAnalyzeAndGetCode() {
        assertEquals(10,developerTask02.doAnalyzeAndGetCode(7));
        assertEquals(-2,developerTask02.doAnalyzeAndGetCode(-5));
    }

    @Test
    public void isAlfaNumberCategory() {
        assertTrue(developerTask02.isAlfaNumberCategory(30));
        assertFalse(developerTask02.isAlfaNumberCategory(2));
    }

    @Test
    public void doAnalyzeBettaSectorCode() {
        assertEquals(73, developerTask02.doAnalyzeBettaSectorCode(2));
        assertEquals(72, developerTask02.doAnalyzeBettaSectorCode(1));
        assertEquals(71, developerTask02.doAnalyzeBettaSectorCode(0));
    }
}