package com.swampus.excercise.task;

import com.swampus.excercise.not.modify.UCSMPlatformConnector;
import com.swampus.excercise.not.modify.UCSMPlatformConnectorProxy;

import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;


import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LeadTask03Test {

    private UCSMPlatformConnector ucsmPlatformConnector =  mock(UCSMPlatformConnector.class);
    private LeadTask03 leadTask03 = new LeadTask03();

    @Before
    public void setUp(){
        Whitebox.setInternalState(leadTask03, "ucsmPlatformConnector",ucsmPlatformConnector);
    }

    @Test
    public void extractUserNameFromHeader() {
        when(ucsmPlatformConnector.getHeader()).thenReturn("[GHAHBSGYB^&F%&A(NBOBA^%FGY(UIASD]");
        assertEquals("[GHA", leadTask03.extractUserNameFromHeader());
    }

    @Test
    public void extractAlfaFromContent() {
        when(ucsmPlatformConnector.getContent("A")).thenReturn("[6882763872387872938972368236872348792436823623862348768234]");
        assertEquals("27",leadTask03.extractAlfaFromContent("A"));

        when(ucsmPlatformConnector.getContent("B")).thenReturn("[111111222222222233333333333]");
        assertEquals("11",leadTask03.extractAlfaFromContent("B"));

        when(ucsmPlatformConnector.getContent("C")).thenReturn("[C22CCCCC22222CCCCC]");
        assertEquals("CC",leadTask03.extractAlfaFromContent("C"));

    }
}